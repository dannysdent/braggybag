<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    <title>@yield('page_title')</title>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="content-language" content="{{ app()->getLocale() }}">

    <!-- <link rel="stylesheet" href="{{ bagisto_asset('css/shop.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/webkul/ui/assets/css/ui.css') }}"> -->

    <link rel="stylesheet" href="{{ bagisto_asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('fonts/themify/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('fonts/elegant-font/html-css/style.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('vendor/animate/animate.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('vendor/css-hamburgers/hamburgers.min.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('vendor/animsition/css/animsition.min.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('vendor/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('vendor/daterangepicker/daterangepicker.css') }}"> 
    <link rel="stylesheet" href="{{ bagisto_asset('vendor/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('vendor/noui/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('vendor/lightbox2/css/lightbox.min.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('css/util.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('css/main.css') }}">

    <!-- <link rel="icon" type="image/png" href="images/icons/favicon.png"/> -->

    @if ($favicon = core()->getCurrentChannel()->favicon_url)
        <link rel="icon" sizes="16x16" href="{{ $favicon }}" />
    @else
        <link rel="icon" sizes="16x16" href="{{ bagisto_asset('images/favicon.ico') }}" />
    @endif

    @yield('head')

    @section('seo')
        @if (! request()->is('/'))
            <meta name="description" content="{{ core()->getCurrentChannel()->description }}"/>
        @endif
    @show


</head>


<body class="rtl animsition" @if (core()->getCurrentLocale()->direction == 'rtl') @endif style="scroll-behavior: smooth;">

    {!! view_render_event('bagisto.shop.layout.body.before') !!}


            {!! view_render_event('bagisto.shop.layout.header.before') !!}

            @include('shop::layouts.header.index')

            {!! view_render_event('bagisto.shop.layout.header.after') !!}


            

                {!! view_render_event('bagisto.shop.layout.content.before') !!}

                @yield('content-wrapper')

                {!! view_render_event('bagisto.shop.layout.content.after') !!}

            

       

        {!! view_render_event('bagisto.shop.layout.footer.before') !!}

        @include('shop::layouts.footer.footer')

        {!! view_render_event('bagisto.shop.layout.footer.after') !!}


        <overlay-loader :is-open="show_loader"></overlay-loader>
    </div>

    <script type="text/javascript">
        window.flashMessages = [];

        @if ($success = session('success'))
            window.flashMessages = [{'type': 'alert-success', 'message': "{{ $success }}" }];
        @elseif ($warning = session('warning'))
            window.flashMessages = [{'type': 'alert-warning', 'message': "{{ $warning }}" }];
        @elseif ($error = session('error'))
            window.flashMessages = [{'type': 'alert-error', 'message': "{{ $error }}" }
            ];
        @elseif ($info = session('info'))
            window.flashMessages = [{'type': 'alert-info', 'message': "{{ $info }}" }
            ];
        @endif

        window.serverErrors = [];
        @if(isset($errors))
            @if (count($errors))
                window.serverErrors = @json($errors->getMessages());
            @endif
        @endif
    </script>



    <script type="text/javascript" src="{{ bagisto_asset('vendor/jquery/jquery-3.2.1.min.js') }}"></script>

    <script type="text/javascript" src="{{ bagisto_asset('vendor/animsition/js/animsition.min.js') }}"></script>
    <script type="text/javascript" src="{{ bagisto_asset('vendor/bootstrap/js/popper.js') }}"></script>
    <script type="text/javascript" src="{{ bagisto_asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ bagisto_asset('vendor/select2/select2.min.js') }}"></script>
    <script type="text/javascript">
        $(".selection-1").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });
    </script>
    <script type="text/javascript" src="{{ bagisto_asset('vendor/slick/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ bagisto_asset('js/slick-custom.js') }}"></script>
    <script type="text/javascript" src="{{ bagisto_asset('vendor/countdowntime/countdowntime.js') }}"></script>
    <script type="text/javascript" src="{{ bagisto_asset('vendor/lightbox2/js/lightbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ bagisto_asset('vendor/sweetalert/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        $('.block2-btn-addcart').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to cart !", "success");
            });
        });

        $('.block2-btn-addwishlist').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to wishlist !", "success");
            });
        });
    </script>
    <!-- <script type="text/javascript" src="{{ bagisto_asset('vendor/noui/nouislider.min.js') }}"></script>
    <script type="text/javascript">
        /*[ No ui ]
        ===========================================================*/
        var filterBar = document.getElementById('filter-bar');

        noUiSlider.create(filterBar, {
            start: [ 50, 200 ],
            connect: true,
            range: {
                'min': 50,
                'max': 200
            }
        });

        var skipValues = [
        document.getElementById('value-lower'),
        document.getElementById('value-upper')
        ];

        filterBar.noUiSlider.on('update', function( values, handle ) {
            skipValues[handle].innerHTML = Math.round(values[handle]) ;
        });
    </script> -->
    <script type="text/javascript" src="{{ bagisto_asset('vendor/parallax100/parallax100.js') }}"></script>
    <script type="text/javascript">
        $('.parallax100').parallax100();
    </script>

    <script type="text/javascript" src="{{ bagisto_asset('js/main.js') }}"></script>

    <script type="text/javascript" src="{{ bagisto_asset('js/shop.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/webkul/ui/assets/js/ui.js') }}"></script>


   @stack('scripts')

    {!! view_render_event('bagisto.shop.layout.body.after') !!}

    <div class="modal-overlay"></div>

</body>

</html>