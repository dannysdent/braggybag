<?php
    $term = request()->input('term');

    if (! is_null($term)) {
        $serachQuery = 'term='.request()->input('term');
    }
?>

<?php
    $categories = [];

    foreach (app('Webkul\Category\Repositories\CategoryRepository')->getVisibleCategoryTree(core()->getCurrentChannel()->root_category_id) as $category){
    if ($category->slug)
        array_push($categories, $category);
        }
?>
@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')

<?php $cart = cart()->getCart(); ?>


    <!-- header fixed -->
    <div class="wrap_header fixed-header2 trans-0-4">
        <!-- Logo -->
        <a href="/" class="logo">
            <img src="{{ bagisto_asset('imagess/icons/logo-b.png') }}" alt="IMG-LOGO">
        </a>

        <!-- Menu -->
        <div class="wrap_menu">
            <nav class="menu">
                <ul class="main_menu">
                   
                            <li class="p-b-9">
                                <a href="/new-arrivals" class="s-text7">New Arrivals</a>
                            </li>
                            <li class="p-b-9 sale-noti">
                                <a href="/sale" class="s-text7 sale">Sale</a>
                            </li>
                            <li class="p-b-9">
                                <a href="/handbag" class="s-text7">Handbags</a>
                            </li>
                            <li class="p-b-9">
                                <a href="tote-bag" class="s-text7">Tote Bags</a>
                            </li>
                            <li class="p-b-9">
                                <a href="crossbody-bag" class="s-text7">Crossbody Bags</a>
                            </li>
                            <li class="p-b-9">
                                <a href="diaper-bag" class="s-text7">Diaper Bags</a>
                            </li>
        
                </ul>
            </nav>
        </div>

        <!-- Header Icon -->
        <div class="header-icons">
            <div class="header-wrapicon2">
                <img src="{{ bagisto_asset('imagess/icons/icon-header-01.png') }}" class="header-icon1 js-show-header-dropdown" alt="ICON">

                <!-- Header cart noti -->
                <div class="header-cart header-dropdown">
                    <b>Account</b>
                    <p>Manage Cart,Wishlist, Orders.</p>
                    <br>
                    <div class="header-cart-buttons">
                        <div class="header-cart-wrapbtn">
                            <!-- Button -->
                            <a href="{{ route('customer.register.index') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                Sign Up
                            </a>
                        </div>

                        <div class="header-cart-wrapbtn">
                            <!-- Button -->
                            <a href="{{ route('customer.session.index') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                Sign In
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <span class="linedivide1"></span>
            @if ($cart)
                    <?php $items = $cart->items; ?>

                    <div class="header-wrapicon2 m-r-13">
                        {!! view_render_event('bagisto.shop.layout.header.cart-item.before') !!}
                        <img src="{{ bagisto_asset('imagess/icons/icon-header-02.png') }}" class="header-icon1 js-show-header-dropdown" alt="ICON">

                        <span class="header-icons-noti">{{ $cart->items->count() }}</span>

                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown">
                            <ul class="header-cart-wrapitem">
                                @foreach ($items as $item)
                                <li class="header-cart-item">
                                    <div class="header-cart-item-img">
                                        @php
                                        $images = $item->product->getTypeInstance()->getBaseImage($item);
                                    @endphp
                                        <img src="{{ $images['small_image_url'] }}" alt="IMG">
                                    </div>

                                    <div class="header-cart-item-txt">
                                        <a href="#" class="header-cart-item-name">
                                            {!! view_render_event('bagisto.shop.checkout.cart-mini.item.name.before', ['item' => $item]) !!}

                                            {{ $item->name }}

                                            {!! view_render_event('bagisto.shop.checkout.cart-mini.item.name.after', ['item' => $item]) !!}
                                        </a>

                                        <span class="header-cart-item-info">
                                            {!! view_render_event('bagisto.shop.checkout.cart-mini.item.price.before', ['item' => $item]) !!}

                                            {{ core()->currency($item->base_total) }}

                                            {!! view_render_event('bagisto.shop.checkout.cart-mini.item.price.after', ['item' => $item]) !!}
                                        </span>
                                    </div>
                                </li>
                                @endforeach
                            </ul>

                            <div class="header-cart-total">
                            {{ __('shop::app.checkout.cart.cart-subtotal') }} -

                            {!! view_render_event('bagisto.shop.checkout.cart-mini.subtotal.before', ['cart' => $cart]) !!}

                            {{ core()->currency($cart->base_sub_total) }}

                            {!! view_render_event('bagisto.shop.checkout.cart-mini.subtotal.after', ['cart' => $cart]) !!}
                            </div>

                            <div class="header-cart-buttons">
                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="{{ route('shop.checkout.cart.index') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        View Cart
                                    </a>
                                </div>

                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="{{ route('shop.checkout.onepage.index') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Check Out
                                    </a>
                                </div>
                            </div>
                        </div>

                        @else
                        <img src="{{ bagisto_asset('imagess/icons/icon-header-02.png') }}" class="header-icon1" alt="ICON">

                    @endif
                        

                        {!! view_render_event('bagisto.shop.layout.header.cart-item.after') !!}
                    </div>

        </div>
    </div>

    <!-- top noti -->
    <div class="flex-c-m size22 bg1 s-text21 pos-relative">
        20% off everything!
        <a href="/sale" class="s-text22 hov6 p-l-5">
            Shop Now
        </a>

        <!-- <button class="flex-c-m pos2 size23 colorwhite eff3 trans-0-4 btn-romove-top-noti">
            <i class="fa fa-remove fs-13" aria-hidden="true"></i>
        </button> -->
    </div>

    <!-- Header -->
    <header class="header2">
        <!-- Header desktop -->
        <div class="container-menu-header-v2 p-t-26">
            <div class="topbar2">
                <div class="topbar-social">
                    <a href="#" class="topbar-social-item fa fa-facebook"></a>
                    <a href="#" class="topbar-social-item fa fa-instagram"></a>
                    <a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
                    <a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
                    <a href="#" class="topbar-social-item fa fa-youtube-play"></a>
                </div>

                <!-- Logo2 -->
                <a href="/" class="logo2">
                    <img src="{{ bagisto_asset('imagess/icons/logo-b.png') }}" alt="IMG-LOGO">
                </a>

                <div class="topbar-child2">
                    <span class="topbar-email">
                        
                    </span>

                    <div class="topbar-language rs1-select2">
                        <select class="selection-1" name="time" onchange="window.location.href = this.value">
                            @foreach (core()->getCurrentChannel()->currencies as $currency)
                                    @if (isset($serachQuery))
                                        <option value="?{{ $serachQuery }}&currency={{ $currency->code }}" {{ $currency->code == core()->getCurrentCurrencyCode() ? 'selected' : '' }}>{{ $currency->code }}</option>
                                    @else
                                        <option value="?currency={{ $currency->code }}" {{ $currency->code == core()->getCurrentCurrencyCode() ? 'selected' : '' }}>{{ $currency->code }}</option>
                                    @endif
                                @endforeach
                        </select>
                    </div>



                    <!--  -->
                    <div class="header-wrapicon2 dis-block m-l-30">
                        <img src="{{ bagisto_asset('imagess/icons/icon-header-01.png') }}" class="header-icon1 js-show-header-dropdown" alt="ICON">

                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown">
                            <b>Account</b>
                            <p>Manage Cart,Wishlist, Orders.</p>
                            <br>
                            <div class="header-cart-buttons">
                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="{{ route('customer.register.index') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Sign Up
                                    </a>
                                </div>

                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="{{ route('customer.session.index') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Sign In
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <span class="linedivide1"></span>
                    @if ($cart)
                    <?php $items = $cart->items; ?>

                    <div class="header-wrapicon2 m-r-13">
                        {!! view_render_event('bagisto.shop.layout.header.cart-item.before') !!}
                        <img src="{{ bagisto_asset('imagess/icons/icon-header-02.png') }}" class="header-icon1 js-show-header-dropdown" alt="ICON">

                        <span class="header-icons-noti">{{ $cart->items->count() }}</span>

                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown">
                            <ul class="header-cart-wrapitem">
                                @foreach ($items as $item)
                                <li class="header-cart-item">
                                    <div class="header-cart-item-img">
                                        @php
                                        $images = $item->product->getTypeInstance()->getBaseImage($item);
                                    @endphp
                                        <img src="{{ $images['small_image_url'] }}" alt="IMG">
                                    </div>

                                    <div class="header-cart-item-txt">
                                        <a href="#" class="header-cart-item-name">
                                            {!! view_render_event('bagisto.shop.checkout.cart-mini.item.name.before', ['item' => $item]) !!}

                                            {{ $item->name }}

                                            {!! view_render_event('bagisto.shop.checkout.cart-mini.item.name.after', ['item' => $item]) !!}
                                        </a>

                                        <span class="header-cart-item-info">
                                            {!! view_render_event('bagisto.shop.checkout.cart-mini.item.price.before', ['item' => $item]) !!}

                                            {{ core()->currency($item->base_total) }}

                                            {!! view_render_event('bagisto.shop.checkout.cart-mini.item.price.after', ['item' => $item]) !!}
                                        </span>
                                    </div>
                                </li>
                                @endforeach
                            </ul>

                            <div class="header-cart-total">
                            {{ __('shop::app.checkout.cart.cart-subtotal') }} -

                            {!! view_render_event('bagisto.shop.checkout.cart-mini.subtotal.before', ['cart' => $cart]) !!}

                            {{ core()->currency($cart->base_sub_total) }}

                            {!! view_render_event('bagisto.shop.checkout.cart-mini.subtotal.after', ['cart' => $cart]) !!}
                            </div>

                            <div class="header-cart-buttons">
                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="{{ route('shop.checkout.cart.index') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        View Cart
                                    </a>
                                </div>

                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="{{ route('shop.checkout.onepage.index') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Check Out
                                    </a>
                                </div>
                            </div>
                        </div>

                        @else
                        <img src="{{ bagisto_asset('imagess/icons/icon-header-02.png') }}" class="header-icon1" alt="ICON">
                    @endif
                        

                        {!! view_render_event('bagisto.shop.layout.header.cart-item.after') !!}
                    </div>
                </div>
            </div>

            <div class="wrap_menu">
            <nav class="menu">
                <ul class="main_menu">
                    

                            <li class="p-b-9">
                                <a href="{{ route('shop.productOrCategory.index', $category->slug) }}" class="s-text7">New Arrivals</a>
                            </li>
                            <li class="p-b-9 sale-noti">
                                <a href="{{ route('shop.productOrCategory.index', $category->slug) }}" class="s-text7 sale">Sale</a>
                            </li>
                            <li class="p-b-9">
                                <a href="{{ route('shop.productOrCategory.index', $category->slug) }}" class="s-text7">Handbags</a>
                            </li>
                            <li class="p-b-9">
                                <a href="{{ route('shop.productOrCategory.index', $category->slug) }}" class="s-text7">Tote Bags</a>
                            </li>
                            <li class="p-b-9">
                                <a href="{{ route('shop.productOrCategory.index', $category->slug) }}" class="s-text7">Crossbody Bags</a>
                            </li>
                            <li class="p-b-9">
                                <a href="{{ route('shop.productOrCategory.index', $category->slug) }}" class="s-text7">Diaper Bags</a>
                            </li>

               
                </ul>
            </nav>
        </div>
        </div>

        <!-- Header Mobile -->
        <div class="wrap_header_mobile">
            <!-- Logo moblie -->
            <a href="/" class="logo-mobile">
                <img src="{{ bagisto_asset('imagess/icons/logo-b.png') }}" alt="IMG-LOGO">
            </a>

            <!-- Button show menu -->
            <div class="btn-show-menu">
                <!-- Header Icon mobile -->
                <div class="header-icons-mobile">
                        <div class="header-wrapicon2 dis-block m-l-30">
                        <img src="{{ bagisto_asset('imagess/icons/icon-header-01.png') }}" class="header-icon1 js-show-header-dropdown" alt="ICON">

                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown">
                            <b>Account</b>
                            <p>Manage Cart,Wishlist, Orders.</p>
                            <br>
                            <div class="header-cart-buttons">
                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="{{ route('customer.register.index') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Sign Up
                                    </a>
                                </div>

                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="{{ route('customer.session.index') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Sign In
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <span class="linedivide2"></span>

                    @if ($cart)
                    <?php $items = $cart->items; ?>

                    <div class="header-wrapicon2 m-r-13">
                        {!! view_render_event('bagisto.shop.layout.header.cart-item.before') !!}
                        <img src="{{ bagisto_asset('imagess/icons/icon-header-02.png') }}" class="header-icon1 js-show-header-dropdown" alt="ICON">

                        <span class="header-icons-noti">{{ $cart->items->count() }}</span>

                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown">
                            <ul class="header-cart-wrapitem">
                                @foreach ($items as $item)
                                <li class="header-cart-item">
                                    <div class="header-cart-item-img">
                                        @php
                                        $images = $item->product->getTypeInstance()->getBaseImage($item);
                                    @endphp
                                        <img src="{{ $images['small_image_url'] }}" alt="IMG">
                                    </div>

                                    <div class="header-cart-item-txt">
                                        <a href="#" class="header-cart-item-name">
                                            {!! view_render_event('bagisto.shop.checkout.cart-mini.item.name.before', ['item' => $item]) !!}

                                            {{ $item->name }}

                                            {!! view_render_event('bagisto.shop.checkout.cart-mini.item.name.after', ['item' => $item]) !!}
                                        </a>

                                        <span class="header-cart-item-info">
                                            {!! view_render_event('bagisto.shop.checkout.cart-mini.item.price.before', ['item' => $item]) !!}

                                            {{ core()->currency($item->base_total) }}

                                            {!! view_render_event('bagisto.shop.checkout.cart-mini.item.price.after', ['item' => $item]) !!}
                                        </span>
                                    </div>
                                </li>
                                @endforeach
                            </ul>

                            <div class="header-cart-total">
                            {{ __('shop::app.checkout.cart.cart-subtotal') }} -

                            {!! view_render_event('bagisto.shop.checkout.cart-mini.subtotal.before', ['cart' => $cart]) !!}

                            {{ core()->currency($cart->base_sub_total) }}

                            {!! view_render_event('bagisto.shop.checkout.cart-mini.subtotal.after', ['cart' => $cart]) !!}
                            </div>

                            <div class="header-cart-buttons">
                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="{{ route('shop.checkout.cart.index') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        View Cart
                                    </a>
                                </div>

                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="{{ route('shop.checkout.onepage.index') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Check Out
                                    </a>
                                </div>
                            </div>
                        </div>

                        @else
                        <img src="{{ bagisto_asset('imagess/icons/icon-header-02.png') }}" class="header-icon1" alt="ICON">
                    @endif
                        

                        {!! view_render_event('bagisto.shop.layout.header.cart-item.after') !!}
                    </div>
                </div>

                <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </div>
            </div>
        </div>

        <!-- Menu Mobile -->
        <div class="wrap-side-menu" >
            <nav class="menu">
                <ul class="main_menu">
                    
                            <li class="p-b-9">
                                <a href="{{ route('shop.productOrCategory.index', $category->slug) }}" class="s-text7">New Arrivals</a>
                            </li>
                            <li class="p-b-9 sale-noti">
                                <a href="{{ route('shop.productOrCategory.index', $category->slug) }}" class="s-text7 sale">Sale</a>
                            </li>
                            <li class="p-b-9">
                                <a href="{{ route('shop.productOrCategory.index', $category->slug) }}" class="s-text7">Handbags</a>
                            </li>
                            <li class="p-b-9">
                                <a href="{{ route('shop.productOrCategory.index', $category->slug) }}" class="s-text7">Tote Bags</a>
                            </li>
                            <li class="p-b-9">
                                <a href="{{ route('shop.productOrCategory.index', $category->slug) }}" class="s-text7">Crossbody Bags</a>
                            </li>
                            <li class="p-b-9">
                                <a href="{{ route('shop.productOrCategory.index', $category->slug) }}" class="s-text7">Diaper Bags</a>
                            </li>

               
                </ul>
            </nav>
        </div>
    </header>   

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tensorflow-models/mobilenet"></script>

    <!-- <script type="text/x-template" id="image-search-component-template">
        <div>
            <label class="image-search-container" for="image-search-container">
                <i class="icon camera-icon"></i>

                <input type="file" id="image-search-container" ref="image_search_input" v-on:change="uploadImage()"/>

                <img id="uploaded-image-url" :src="uploaded_image_url"/>
            </label>
        </div>
    </script> -->

    <!-- <script>

        Vue.component('image-search-component', {

            template: '#image-search-component-template',

            data: function() {
                return {
                    uploaded_image_url: ''
                }
            },

            methods: {
                uploadImage: function() {
                    var imageInput = this.$refs.image_search_input;

                    if (imageInput.files && imageInput.files[0]) {
                        if (imageInput.files[0].type.includes('image/')) {
                            var self = this;

                            self.$root.showLoader();

                            var formData = new FormData();

                            formData.append('image', imageInput.files[0]);

                            axios.post("{{ route('shop.image.search.upload') }}", formData, {headers: {'Content-Type': 'multipart/form-data'}})
                                .then(function(response) {
                                    self.uploaded_image_url = response.data;

                                    var net;

                                    async function app() {
                                        var analysedResult = [];

                                        var queryString = '';

                                        net = await mobilenet.load();

                                        const imgElement = document.getElementById('uploaded-image-url');

                                        try {
                                            const result = await net.classify(imgElement);

                                            result.forEach(function(value) {
                                                queryString = value.className.split(',');

                                                if (queryString.length > 1) {
                                                    analysedResult = analysedResult.concat(queryString)
                                                } else {
                                                    analysedResult.push(queryString[0])
                                                }
                                            });
                                        } catch (error) {
                                            self.$root.hideLoader();

                                            window.flashMessages = [
                                                {
                                                    'type': 'alert-error',
                                                    'message': "{{ __('shop::app.common.error') }}"
                                                }
                                            ];

                                            self.$root.addFlashMessages();
                                        };

                                        localStorage.searched_image_url = self.uploaded_image_url;

                                        queryString = localStorage.searched_terms = analysedResult.join('_');

                                        self.$root.hideLoader();

                                        window.location.href = "{{ route('shop.search.index') }}" + '?term=' + queryString + '&image-search=1';
                                    }

                                    app();
                                })
                                .catch(function(error) {
                                    self.$root.hideLoader();

                                    window.flashMessages = [
                                        {
                                            'type': 'alert-error',
                                            'message': "{{ __('shop::app.common.error') }}"
                                        }
                                    ];

                                    self.$root.addFlashMessages();
                                });
                        } else {
                            imageInput.value = '';

                            alert('Only images (.jpeg, .jpg, .png, ..) are allowed.');
                        }
                    }
                }
            }
        });

    </script> -->

    <script>
        $(document).ready(function() {

            $('body').delegate('#search, .icon-menu-close, .icon.icon-menu', 'click', function(e) {
                toggleDropdown(e);
            });

            @auth('customer')
                @php
                    $compareCount = app('Webkul\Velocity\Repositories\VelocityCustomerCompareProductRepository')
                        ->count([
                            'customer_id' => auth()->guard('customer')->user()->id,
                        ]);
                @endphp

                let comparedItems = JSON.parse(localStorage.getItem('compared_product'));
                $('#compare-items-count').html({{ $compareCount }});
            @endauth

            @guest('customer')
                let comparedItems = JSON.parse(localStorage.getItem('compared_product'));
                $('#compare-items-count').html(comparedItems ? comparedItems.length : 0);
            @endguest

            function toggleDropdown(e) {
                var currentElement = $(e.currentTarget);

                if (currentElement.hasClass('icon-search')) {
                    currentElement.removeClass('icon-search');
                    currentElement.addClass('icon-menu-close');
                    $('#hammenu').removeClass('icon-menu-close');
                    $('#hammenu').addClass('icon-menu');
                    $("#search-responsive").css("display", "block");
                    $("#header-bottom").css("display", "none");
                } else if (currentElement.hasClass('icon-menu')) {
                    currentElement.removeClass('icon-menu');
                    currentElement.addClass('icon-menu-close');
                    $('#search').removeClass('icon-menu-close');
                    $('#search').addClass('icon-search');
                    $("#search-responsive").css("display", "none");
                    $("#header-bottom").css("display", "block");
                } else {
                    currentElement.removeClass('icon-menu-close');
                    $("#search-responsive").css("display", "none");
                    $("#header-bottom").css("display", "none");
                    if (currentElement.attr("id") == 'search') {
                        currentElement.addClass('icon-search');
                    } else {
                        currentElement.addClass('icon-menu');
                    }
                }
            }
        });
    </script>
@endpush
