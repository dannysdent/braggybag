{!! view_render_event('bagisto.shop.products.price.before', ['product' => $product]) !!}

<span class="block2-price m-text6 p-r-5">
 {!! $product->getTypeInstance()->getPriceHtml() !!}                     
</span>

{!! view_render_event('bagisto.shop.products.price.after', ['product' => $product]) !!}