{!! view_render_event('bagisto.shop.products.list.card.before', ['product' => $product]) !!}

    @inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')

    <?php $productBaseImage = $productImageHelper->getProductBaseImage($product); ?>
                    <div class="col-sm-12 col-md-6 col-lg-4 p-b-50" id="main">
                        <!-- Block2 -->
                        <div class="block2">
                            <div class="block2-img wrap-pic-w of-hidden pos-relative">
                                <a href="{{ route('shop.productOrCategory.index', $product->url_key) }}" title="{{ $product->name }}">
                                    <img src="{{ $productBaseImage['medium_image_url'] }}" onerror="this.src='{{ asset('vendor/webkul/ui/assets/images/product/meduim-product-placeholder.png') }}'"/>
                                </a>

                                <div class="block2-overlay trans-0-4">
                                    <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                        <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                        <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                    </a>

                                    <div class="block2-btn-addcart w-size1 trans-0-4">
                                        <!-- Button -->
                                        
                                            {!! view_render_event('bagisto.shop.products.add_to_cart.before', ['product' => $product]) !!}

                                            <form action="{{ route('cart.add', $product->product_id) }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="product_id" value="{{ $product->product_id }}">
                                            <input type="hidden" name="quantity" value="1">
                                            <button type="submit" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" {{ ! $product->isSaleable() ? 'disabled' : '' }}>
                                                {{ ($product->type == 'booking') ?  __('shop::app.products.book-now') :  __('shop::app.products.add-to-cart') }}
                                            </button>
                                        </form>
                                            {!! view_render_event('bagisto.shop.products.add_to_cart.after', ['product' => $product]) !!}
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="block2-txt p-t-20">
                                <a href="{{ route('shop.productOrCategory.index', $product->url_key) }}" title="{{ $product->name }}">
                                    <span>
                                        {{ $product->name }}
                                    </span>
                                </a>
                                <br>

                            
                                @include ('shop::products.price', ['product' => $product])
                                
                            </div>
                        </div>
                </div>

            {!! view_render_event('bagisto.shop.products.list.card.after', ['product' => $product]) !!}
